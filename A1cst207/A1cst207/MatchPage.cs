﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;


namespace A1cst207
{
    //This page is to show the matches page

    public class MatchPage : ContentPage
    {

        //static list that can be accessed all across the project
        public static ObservableCollection<Matches> list;
        //static first name and last name that can be use the all across the class, because we
        //want to the opponent name on top of each view cell. the match database don't contain these
        //properties, so we just get the data from the mainpage, and keep them as these
        internal static string first;
        internal static string last;
        public MatchPage(Opponents opponent)
        {

            //get the name from the data that transfer from the last page
            last = opponent.lName;
            first = opponent.fName;
          
            //set up a newMatches for the listview tapping event, use this matches object to 
            //decide if the add new matches button is add a new object or update a existing object
            Matches newMatches = null;
            //get the matches list from the database that the opponent ID is equal with the 
            //opponent's ID
            List<Matches> matches = MainPage.mydb.GetMatchesItem(opponent.ID);
            
            list = new ObservableCollection<Matches>(matches);
            //listview to show to the user
            var listView = new ListView
            {
                ItemsSource = list,
                RowHeight = MatchPageCell.RowHeight,
                HeightRequest = 900,
                ItemTemplate = new DataTemplate(typeof(MatchPageCell))
            };
            //table view for the user can add new match or update the exist match
            TableView table = new TableView { Intent = TableIntent.Form };
            ViewCell dPicker = new ViewCell();
            DatePicker picker = new DatePicker { Date = DateTime.Now, WidthRequest = 125, Format = "D" };
            dPicker.View = picker;
            EntryCell eComment = new EntryCell { Label = "Comment:", Text = "" };
            ViewCell gCell = new ViewCell();
            Picker gPicker = new Picker
            {
                Title = "Game",
                //the game picker will get the game name from the game table and binding the 
                //picker to the gName property, so it will so the game name when user click the 
                //game box
                ItemsSource = MainPage.mydb.GetGamesItem(),
                ItemDisplayBinding = new Binding("gName"),
            };

            gCell.View = gPicker;
            //switchcell true or false 
            SwitchCell ifWin = new SwitchCell { Text = "Win?" };
            ifWin.On = false;
            TableSection section = new TableSection
            {
                dPicker,
                eComment,
                gCell,
                ifWin
            };
            section.Title = "Add Match";
            table.Root = new TableRoot { section };

            //listview tapping event handler to make the table view populated with the selected items
            listView.ItemTapped += (sender, e) =>
            {
                listView.SelectedItem = null;
                picker.Date = (e.Item as Matches).date;//selected item's date
                eComment.Text = (e.Item as Matches).comments;//selected item's comments
                gPicker.SelectedIndex = (e.Item as Matches).game;//selected item's game
                ifWin.On = (e.Item as Matches).win;//selected item's win status
                newMatches = (Matches)(e.Item);//set up the newMatches as current matches
            };
            Button btnSave = new Button { Text = "Add " };
            btnSave.Clicked += (sender, e) =>
            {
                if (newMatches != null)
                {

                    //if the newMatches is not null, that means the input is exist already, we just want to
                    //update it           
                    newMatches.date = picker.Date;
                    newMatches.comments = eComment.Text;
                    var gameName = gPicker.Items[gPicker.SelectedIndex];//get the game's name
                    Games game = MainPage.mydb.GetGameItem(gameName);//get the game from the gameDatabase
                    var gameNumber = game.ID;//get the gameNumber 
                    newMatches.win = ifWin.On;//get the newMatch's win status

                    MainPage.mydb.SaveMatchItem(newMatches);//update this match to database
                    newMatches = null;//set the newMatch to null
                }
                else
                {
                    //using try in case the user fill nothing and will crash the app
                    try
                    {
                        //if the newMatch is null, that we set up a new Matches, and need to store  to the database
                        var newName = gPicker.Items[gPicker.SelectedIndex];

                        //because the game binding the game name, but in the matches table , we store the 
                        //game ID, so here , we need to transfer the gamename to game id,
                        Games newGame = MainPage.mydb.GetGameItem(newName);
                        var newNumber = newGame.ID;

                        //set up a new instance of matches
                        var newMatches1 = new Matches
                        {
                            oppID = opponent.ID,
                            date = picker.Date,
                            comments = eComment.Text,
                            game = newNumber,
                            win = ifWin.On
                        };
                        //update the list
                        list.Add(newMatches1);
                        //save to the database
                        MainPage.mydb.SaveMatchItem(newMatches1);
                        //make the table view go back to the original status
                        eComment.Text = " ";
                        picker.Date = DateTime.Now;
                        ifWin.On = false;
                        gPicker.SelectedIndex = -1;
                    }
                    catch (Exception ex)
                    {
                        eComment.Text = "please fill the forms";

                    }
                }

            };
            Content = new StackLayout
            {
                Children = {
                    listView,
                    new BoxView { HeightRequest = 2, WidthRequest = 400, Color = Color.Black },
                    table,

                    btnSave
                }
            };

        }
    }

    //viewcell to show the list item for every match
    public class MatchPageCell : ViewCell
    {

        Label labelGame = new Label { HorizontalOptions = LayoutOptions.StartAndExpand };
        public const int RowHeight = 95;
        public MatchPageCell()
        {

            var firstName = new Label { FontAttributes = FontAttributes.Bold };
            firstName.Text = MatchPage.first;
            var lastName = new Label { FontAttributes = FontAttributes.Bold };
            lastName.Text = MatchPage.last;
            var nameStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Children = { firstName, lastName }
            };

            var date = new Label { HorizontalOptions = LayoutOptions.StartAndExpand };
            date.SetBinding(Label.TextProperty, "date", stringFormat: "{0:D}");
            var comments = new Label { HorizontalOptions = LayoutOptions.EndAndExpand };
            comments.SetBinding(Label.TextProperty, "comments");
            var dateStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { date, comments }
            };

            var winLabel = new Label { HorizontalOptions = LayoutOptions.End, Text = "Win?" };
            var winResult = new Switch { HorizontalOptions = LayoutOptions.End };
            winResult.SetBinding(Switch.IsToggledProperty, new Binding("win"));

            var gameStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { labelGame, winLabel, winResult }
            };
            View = new StackLayout
            {
                Spacing = 2,
                Padding = 5,
                Children = { nameStack, dateStack, gameStack }
            };
            //set up event handler for delete the match item
            MenuItem mi = new MenuItem { Text = "Delete", IsDestructive = true };
            ContextActions.Add(mi);


            mi.Clicked += (sender, e) =>
            {


                ListView parent = (ListView)this.Parent;
                ObservableCollection<Matches> list = (ObservableCollection<Matches>)parent.ItemsSource;
                //get the current match object from the binding context
                Matches match = (Matches)this.BindingContext;
                //remove this match from the list
                list.Remove((Matches)this.BindingContext);
                int matchID = match.ID;
                //delete the matches from the matches database 
                MainPage.mydb.DeleteMatchItem(match);


            };


        }

        //this method I just want to get the name, because the match table store the game as gameID
        //but we need the game name, so through the onBindingContextChanged event, we can get the 
        //individual items, and get the game ID, furthur to get the name from the database
        protected override void OnBindingContextChanged()
        {
            //use try catch to catch the exception otherwise the app will crash
            try
            {
                //new instance matche from bindingcontext
                Matches match = (Matches)this.BindingContext;
                var gameID = match.game; //get the binding matches' gameID
                Games game = MainPage.mydb.GetGameItem(gameID); // get the game in game database
                var gameName = game.gName;
                labelGame.Text = gameName;//set the label as the gameName


                base.OnBindingContextChanged();
            }
            catch (Exception e) { }
        }

    }
}