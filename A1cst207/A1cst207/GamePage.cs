﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace A1cst207
{
    // This class will show the game category to the user, just for view, no any other event
    public class GamePage : ContentPage
    {

        public static ObservableCollection<Games> list;

        public GamePage()
        {
            //catch an exception in case when drop all the table, because it has no been refresh, 
            //will crash, so catch this exception
            try
            {
                //get all the games from the database 
                List<Games> games = MainPage.mydb.GetGamesItem();
                list = new ObservableCollection<Games>(games);

                //bindind all the game to the listView
                var listView = new ListView
                {
                    ItemsSource = list,
                    RowHeight = GamePageCell.RowHeight,

                    HeightRequest = 900,
                    ItemTemplate = new DataTemplate(typeof(GamePageCell))
                };

                Content = new StackLayout
                {
                    Children = {
                    listView
                }
                };
            }

            catch (Exception e)
            {
                Content = new StackLayout
                {
                    Children = {
                    new Label{Text="Please refresh the database to ge the data"}
                }
                };
            }
        }
    }
    public class GamePageCell : ViewCell
    {
        public static int num;// use static, so that it can be access by the other class
        public const int RowHeight = 95;
        Label numberTotal = new Label();
        public GamePageCell()
        {
            //set name, desc, rate,and the total matches in the viewcell
            var name = new Label { FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.StartAndExpand };
            name.SetBinding(Label.TextProperty, "gName");

            var desc = new Label { HorizontalOptions = LayoutOptions.StartAndExpand };
            desc.SetBinding(Label.TextProperty, "desc");
            var rate = new Label { HorizontalOptions = LayoutOptions.End };
            rate.SetBinding(Label.TextProperty, "rating");
            var descStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { desc, rate }
            };

            Label numberMatch = new Label { Text = "#Matches:" };
            var matchStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { numberMatch, numberTotal }
            };

            View = new StackLayout
            {
                Spacing = 2,
                Padding = 5,
                Children = { name, descStack, matchStack }
            };
        }


        //this function is for the calculation field, we want to get the total number matched, so 
        //we have to get the every Game that bind to the viewcell
        protected override void OnBindingContextChanged()
        {
            //current game bind to the currentContext
            Games game = (Games)this.BindingContext;
            //get gameID
            var gameID = game.ID;
            //get all the list that fit the above gameID
            List<Matches> list1 = MainPage.mydb.GetMatchesItem(gameID);
            var number = list1.Count;//get the total number in the list, that is the total matches
            numberTotal.Text = number.ToString(); //set the label's text as the total number        
            base.OnBindingContextChanged();
        }


    }
}
