﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace A1cst207
{
    //this class is to set up the add new Opponent page
    public class OpponentPage : ContentPage
    {
        public OpponentPage()
        {
            // table View for user to fill in the box

            TableView table = new TableView { Intent = TableIntent.Form };

            EntryCell oFirst = new EntryCell { Label = "First Name:", Text = "" };
            EntryCell oLast = new EntryCell { Label = "Last Name:", Text = "" };
            EntryCell oAddress = new EntryCell { Label = "Address:", Text = "" };
            EntryCell oPhone = new EntryCell { Label = "Phone:", Text = "" };
            EntryCell oEmail = new EntryCell { Label = "Email:", Text = "" };


            TableSection section = new TableSection
            {

                oFirst,
                oLast,
                oAddress,
                oPhone,
                oEmail

            };
            section.Title = "Add New Opponent";
            table.Root = new TableRoot { section };

            Button btnSave = new Button { Text = "Save " };


            btnSave.Clicked += (sender, e) =>
            {
                //set up new opponent
                Opponents opponent = new Opponents();
                opponent.fName = oFirst.Text;
                opponent.lName = oLast.Text;
                opponent.address = oAddress.Text;
                opponent.phone = oPhone.Text;
                opponent.email = oEmail.Text;
                //save the new opponent to the database      
                MainPage.mydb.SaveOpponentItem(opponent);
                oFirst.Text = "";
                oLast.Text = "";
                oAddress.Text = "";
                oPhone.Text = "";
                oEmail.Text = "";

                //add the new opponent to the list
                MainPage.list.Add(opponent);
                //go  back to the main page
                Navigation.PopAsync(true);

            };



            Content = new StackLayout
            {
                Children = {table,btnSave

                }
            };
        }
    }
}