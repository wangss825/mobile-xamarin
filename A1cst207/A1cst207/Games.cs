﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace A1cst207
{
    public class Games
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string gName { get; set; }
        public double Rating;
        //public double rating { get; set; }//between 0 and 10

        //rating will throw an exception if the value is greater than 10 or less than 0
        public double rating
        {
            get { return this.Rating; }
            set
            {
                if (value > 10 || value < 0)
                {
                    throw new Exception("The rating must between 0 and 10");
                }
                else
                {
                    this.Rating = value;
                }
            }
        }

        public string desc { get; set; }
    }
}
