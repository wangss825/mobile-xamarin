﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace A1cst207
{
    public class Matches : INotifyPropertyChanged
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public int oppID { get; set; }
        public DateTime Date;
        public DateTime date
        {

            get { return this.Date; }
            set
            {
                if (value != this.Date)//avoid sending a change notification if no change actually happened
                {
                    this.Date = value;
                    OnPropertyChanged();
                }

            }
        }

        public string Comments;

        public string comments
        {

            get { return this.Comments; }
            set
            {
                if (value != this.Comments)
                {
                    this.Comments = value;
                    OnPropertyChanged();
                }

            }
        }


        public int Game;
        public int game
        {

            get { return this.Game; }
            set
            {
                if (value != this.Game)
                {
                    this.Game = value;
                    OnPropertyChanged();
                }

            }
        }

        public bool Win;

        public bool win
        {

            get { return this.Win; }
            set
            {
                if (value != this.Win)
                {
                    this.Win = value;
                    OnPropertyChanged();
                }

            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string property = "")
        {
            //CallerMembername will set the parameter to the name of the calling property 
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

        }
    }
}
