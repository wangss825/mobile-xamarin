﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace A1cst207
{
    //this class is to show the setting page, only one button to reset the database
    public class SettingPage : ContentPage
    {

        public SettingPage()
        {

            Button reset = new Button { Text = "Reset" };
            //afte click the button, will drop the three table , and reset the table with the 
            //default data
            reset.Clicked += (sender, e) =>
            {
                MainPage.mydb.dropOpponents();
                MainPage.list.Clear();
                MainPage.mydb.dropMatches();
                MainPage.mydb.dropGames();
                MainPage.mydb = MainPage.Mydb;

                //List<Games> list =  MainPage.mydb.GetGamesItem();
                // GamePage.list = new ObservableCollection<Games>(list);

            };
            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Children = {
                    reset
                }
            };
        }
    }
}
