﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace A1cst207
{
    //database will set up the database, create tables and set up some CRUD methods
    public class Database
    {
        readonly SQLiteConnection db;
        public Database(string dbPath)
        {
            //create database and tables
            db = new SQLiteConnection(dbPath);
            //create table Opponents
            db.CreateTable<Opponents>();
           //if (db.Table<Opponents>().Count() == 0)
           //{
           //     Opponents opponent = new Opponents();
           //     opponent.fName = "Jason";
           //     opponent.lName = "Schmidt";
           //     opponent.phone = "306-659-4629";
           //     opponent.address = "1234 Street";
           //     opponent.email = "abc@saskpolytech.ca";
           //     SaveOpponentItem(opponent);
           //     Opponents opponent1 = new Opponents();
           //     opponent1.fName = "Ernesto";
           //     opponent1.lName = "Basoalto";
           //     opponent1.phone = "306-659-4124";
           //     opponent1.address = "12345 Street";
           //     opponent1.email = "abcd@saskpolytech.ca";
           //    // SaveOpponentItem(opponent1);
           //}
           //create table Matches
            db.CreateTable<Matches>();
            //if (db.Table<Matches>().Count() == 0)
            //{
            //    Matches match = new Matches();
            //    match.oppID = 1;
            //    match.date = new DateTime(2019, 1, 25);
            //    match.comments = "Played poorly";
            //    match.win = false;
            //    match.game = 1;
            //    SaveMatchItem(match);

            //    Matches match1 = new Matches();
            //    match1.oppID = 1;
            //    match1.date = new DateTime(2019, 1, 26);
            //    match1.comments = "Did Ok";
            //    match1.win = true;
            //    match1.game = 2;
            //    SaveMatchItem(match1);
            //}
            //create table Games
            db.CreateTable<Games>();
            if (db.Table<Games>().Count() == 0)
            {
                Games game1 = new Games();
                game1.gName = "Cheese";
                game1.rating = 9.5;
                game1.desc = "Simple grid game";

                Games game2 = new Games();
                game2.gName = "Checkers";
                game2.rating = 5;
                game2.desc = "Simpler grid game";

                Games game3 = new Games();
                game3.gName = "Dominoes";
                game3.rating = 6.75;
                game3.desc = "Blocks games";
                SaveGameItem(game1);
                SaveGameItem(game2);
                SaveGameItem(game3);
            }
        }

        //save new opponent to the opponents database
        public int SaveOpponentItem(Opponents opponent)
        {
            if (opponent.ID != 0)
            {
                return db.Update(opponent);
            }
            else
            {
                return db.Insert(opponent);
            }
        }
        //save new match to the database
        public int SaveMatchItem(Matches match)
        {
            if (match.ID != 0)
            {
                return db.Update(match);
            }
            else
            {
                return db.Insert(match);
            }
        }

        //save new Game to the database
        public int SaveGameItem(Games game)
        {
            if (game.ID != 0)
            {
                return db.Update(game);
            }
            else
            {
                return db.Insert(game);
            }
        }

        //get all the opponent list from opponent table
        public List<Opponents> GetOpponentItem()
        {
            return db.Table<Opponents>().ToList<Opponents>();
        }

        //get all the matches list from matches table
        public List<Matches> GetMatchesItem()
        {
            return db.Table<Matches>().ToList<Matches>();
        }
        //get all the games list from game table
        public List<Games> GetGamesItem()
        {
            return db.Table<Games>().ToList<Games>();
        }

        //get the Game that the name matches the game name in the database
        public Games GetGameItem(string name)
        {
            return db.Table<Games>().Where(i => i.gName == name).FirstOrDefault();
        }

        //get the game that the id fit the id in the database

        public Games GetGameItem(int id)
        {
            return db.Table<Games>().Where(i => i.ID == id).FirstOrDefault();
        }

        //get the matches list that the gameID fit the special number
        public List<Matches> GetMatchesItem(int id)
        {
            return db.Table<Matches>().Where(i => i.game == id).ToList<Matches>();
        }

        //get the Opponents that the id will fit the id in the opponent table
        public Opponents GetItem(int id)
        {
            return db.Table<Opponents>().Where(i => i.ID == id).FirstOrDefault();
        }

        //delete one opponent from the opponent table
        public int DeleteOpponentItem(Opponents opponent)
        {
            return db.Delete(opponent);

        }

        //delete the match from the match table
        public int DeleteMatchItem(Matches match)
        {
            return db.Delete(match);

        }
        //delete the match that the id will fit the oppID in the Matches table
        public void DeleteMatchItem(int id)
        {
            db.Query<Matches>("DELETE FROM [Matches] WHERE [oppID] = id");
        }

        //drop Opponents table
        public void dropOpponents()
        {
            db.DropTable<Opponents>();
        }
        //drop Games table
        public void dropGames()
        {
            db.DropTable<Games>();

        }
        //drop Matches table
        public void dropMatches()
        {
            db.DropTable<Matches>();

        }



    }
}
