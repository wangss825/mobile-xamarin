﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace A1cst207
{
     
    //This class will show the mainpage
    public partial class MainPage : ContentPage
    {
        //static database that can be access all cross the project
        public static Database mydb;
        public static Database Mydb
        {
            get
            {
                if (mydb == null)
                {
                    mydb = new Database(DependencyService.Get<IFileHelper>().GetLocalFilePath("assign1.db"));
                }
                return mydb;
            }
        }

        //static ObservableCollection<Opponents> that will be access all across the project
        public static ObservableCollection<Opponents> list;

        public MainPage()
        {
            InitializeComponent();

            //set up the database
            mydb = Mydb;
            //get all the Opponents from database
            List<Opponents> oppo = mydb.GetOpponentItem();
            list = new ObservableCollection<Opponents>(oppo);
           
            //listView to show all the opponents list
            var listView = new ListView
            {
                ItemsSource = list,
                RowHeight = OpponentCell.RowHeight,
                ItemTemplate = new DataTemplate(typeof(OpponentCell))
            };

            //event handler when tapping one item
            listView.ItemTapped += (sender, e) =>
            {
                listView.SelectedItem = null;
                //go to the MatchPage
                Navigation.PushAsync(new MatchPage(e.Item as Opponents));


            };
            Button button = new Button { Text = "Add New Opponent" };
            button.Clicked += (sender, e) =>
            {
                //when click the button will go to the Opponent page, and add the new opponent
                Navigation.PushAsync(new OpponentPage());
            };
            Content = new StackLayout
            {
                Spacing = 15,
                Padding = 25,
                Children =
                {
                     listView,
                     button
                }
            };
        }
    }
    //ViewCell to show the list item according the custom design
    public class OpponentCell : ViewCell
    {
        public const int RowHeight = 55;
        public OpponentCell()
        {
            var id = new Label();
            id.SetBinding(Label.TextProperty, "ID");
            var fNameLabel = new Label();
            fNameLabel.SetBinding(Label.TextProperty, "fName");
            var lNameLabel = new Label();
            lNameLabel.SetBinding(Label.TextProperty, "lName");
            var phoneLabel = new Label { HorizontalOptions = LayoutOptions.EndAndExpand };
            phoneLabel.SetBinding(Label.TextProperty, "phone");

            View = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 2,
                Padding = 5,
                Children = { fNameLabel, lNameLabel, phoneLabel }
            };

            //the sensitive button when that will delete one item from the list
            MenuItem mi = new MenuItem { Text = "Delete", IsDestructive = true };
            mi.Clicked += (sender, e) =>
            {
                ListView parent = (ListView)this.Parent;
                ObservableCollection<Opponents> list = (ObservableCollection<Opponents>)parent.ItemsSource;
                //get the current opponent
                Opponents opp = (Opponents)this.BindingContext;
                int oppID = opp.ID;
                //delete the opponent from the opponent database
                MainPage.mydb.DeleteOpponentItem(opp);
                //delete the matches from the match database.
                MainPage.mydb.DeleteMatchItem(oppID);
                //delete from the Opponent list              
                list.Remove((Opponents)this.BindingContext);
            };
            ContextActions.Add(mi);
        }

    }
}
