using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace A1cst207
{
    //Name: Dan Liu
    //CST: 207
	public partial class App : Application
	{
        public static Database oDatabase;

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());

            //set toolbar game                     
            ToolbarItem game = new ToolbarItem { Text = "Games" };
            //set toolbar setting
            ToolbarItem setting = new ToolbarItem { Text = "Setting" };
            //event handler when click game will go to the gamepage
            game.Clicked += (sender, e) => { MainPage.Navigation.PushAsync(new GamePage()); };
            //event handler when click setting will go to settingpage
            setting.Clicked += (sender, e) => { MainPage.Navigation.PushAsync(new SettingPage()); };


            MainPage.ToolbarItems.Add(game);
            MainPage.ToolbarItems.Add(setting);




        }
    }
}


public interface IFileHelper
{
    string GetLocalFilePath(string filename);
}

