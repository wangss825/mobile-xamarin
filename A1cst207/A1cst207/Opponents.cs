﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace A1cst207
{
    public class Opponents
    {

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string fName { get; set; }
        public string lName { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

        public override string ToString()
        {
            return fName + "     " + lName + "     " + phone;
        }

    }
}
